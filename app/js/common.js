"use strict";

// TABS

function show(el) {
    var el = document.querySelectorAll(el);
    for (let i = 0; i < el.length; i++) {
      if (el[i].style.display == 'none') {
        el[i].style.display = 'block';
      }
    }
    return false;
  }

function hide(el) {
    var el = document.querySelectorAll(el);
    for (let i = 0; i < el.length; i++) {
      if (el[i].style.display == 'block') {
        el[i].style.display = 'none';
      }
    }
    return false;
  }
 
function fadeIn(el) {
    el.style.opacity = 0;
    el.style.display = "block";
    var tick = function() {
    el.style.opacity = +el.style.opacity + 0.05;
    if (+el.style.opacity < 1) {
        (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16)
    }
    };
    tick();
}
 
function addClass(el, className) {
    var el = document.querySelectorAll(el);

    for (let i = 0; i < el.length; i++) {
      if (el.classList) {
        el[i].classList.add(className);
      } else {
        el[i].className += ' ' + className;
      }
    }
  }
 
function removeClass(el, className) {
    var el = document.querySelectorAll(el);
 
    for (let i = 0; i < el.length; i++) {
      if (el[i].classList) {
        el[i].classList.remove(className);
      } else {
        el[i].className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
      }
    }
  }
 
function tabSwitch(el) {
  [].forEach.call(el, function(el) {
    el.addEventListener('click', function() {
     hide(".brochure__content");
 
      var activeTab = this.getAttribute("rel");
      fadeIn(document.querySelector("#"+activeTab));
 
      removeClass(".brochure__tab", "active");
      addClass(".brochure__tab[rel^='" + activeTab + "']", "active");
 
      removeClass(".brochure__mobile-tab", "active");
      addClass(".brochure__mobile-tab[rel^='" + activeTab + "']", "active");
    })
  })
}
 
 // Hide the content and show the first tab
hide(".brochure__content");
fadeIn(document.querySelector("#tab1"));
 
 // Desktop Tabs
 var tabDesktop = document.querySelectorAll(".brochure__tab");
 tabSwitch(tabDesktop);
 
 // Mobile Tabs
 var tabMobile = document.querySelectorAll(".brochure__mobile-tab");
 tabSwitch(tabMobile);

// MASKED PHONE INPUT

var element = document.getElementById('tel');
var maskOptions = {
  mask: '+{7}(000)000-00-00'
};
var mask = new IMask(element, maskOptions);

// YANDEX MAP
var mapTrigger = document.querySelectorAll(".jsShowMap");
mapActive(mapTrigger);

function mapActive(el) {
  [].forEach.call(el, function(el) {
    el.addEventListener('click', function() {
        document.querySelector("#map-menu").innerHTML = '';
        document.querySelector("#map").innerHTML = '';
        showMap();
    })
  })
}

function showMap () {
    ymaps.ready(init);
    function init() {
    // Создание экземпляра карты.
    var myMap = new ymaps.Map('map', {center: [55.753994, 37.622093],zoom: 13, controls: []}, {});

    // Контейнер для меню.
    var menu = $('<div class="form-box__list"/>');

    // Отключение драга на мобильных устройствах.
    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i)
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i)
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i)
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i)
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i)
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows())
        }
        }

    if (isMobile.any()) {
        myMap.behaviors.disable('drag');
    } 

    myMap.controls.add('zoomControl', {
        size: "small",
        position: {right: '15px', bottom: '30px'}
    });

    for (var i = 0, l = groups.length; i < l; i++) {
        createGroup(groups[i]);
    }

    function createGroup (group) {
        var 
        // Коллекция для геообъектов группы.
            collection = new ymaps.GeoObjectCollection(null, { preset: group.style });

        // Добавляем коллекцию на карту.
        myMap.geoObjects.add(collection);
        
        for (var j = 0, m = group.items.length; j < m; j++) {
            createPlacemarks(group, group.items[j], collection);
        }
    }

    function createPlacemarks (group, item, collection) {
        var menuItem = $('<div class="radio-btn form-box__radio"><input type="radio" id="' + item.id + '" name="radio-group"><label for="' + item.id + '">' + item.name + '</label></div>');

        var placemark = new ymaps.Placemark(item.center, {id: item.name});
        // Добавляем метку в коллекцию.
        collection.add(placemark);

        menuItem
        // Добавляем пункт в меню.
            .appendTo(menu)
            // Навешиваем обработчик клика по пункту меню.
            .on('click', function (e) {
                myMap.setCenter(placemark.geometry.getCoordinates(), 13, {duration: 1000});
            });
    }

    menu.appendTo($('#map-menu'));
    $('#radio-1').prop('checked',true);

    // Выставляем масштаб карты чтобы были видны все группы.
    myMap.setBounds(myMap.geoObjects.getBounds(), {checkZoomRange: true, zoomMargin:10});

    }
}
// Группы объектов
var groups = [
    {
        name: "Пункты выдачи заказов",
        style: [ 
        {
            iconLayout: 'default#image',
            iconImageHref: 'img/map/icon.png',
            iconImageSize: [35, 45],
            iconImageOffset: [0, -17]
        }],
        items: [
            {
                id: "radio-1",
                center: [55.801131, 37.508166],
                name: "Пункт выдачи заказов Песчаная улица, дом 13",
            },
            {
                id: "radio-2",
                center: [55.757556, 37.651591],
                name: "Пункт выдачи заказов Подсосенский переулок, 11",
            }
        ]}
];

